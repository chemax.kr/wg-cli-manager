package main

import (
	"encoding/binary"
	"fmt"
	"math/rand"
	"net"
	"os"
	"os/exec"
	"strings"
	"time"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

type Keys struct {
	Pubkey  string
	Privkey string
}

func RandStringBytesRmndr(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}

func pubKeyGen(priv string) string {
	filename := RandStringBytesRmndr(10)
	content := []byte(priv)
	err := os.WriteFile(filename, content, 0644)
	if err != nil {
		fmt.Println(err)
	}
	cmd := "sh"
	args := []string{
		"-c", "wg pubkey < " + filename,
	}
	//fmt.Println(cmd, args)
	result := exec.Command(cmd, args...)
	res, err := result.Output()
	if err != nil {
		fmt.Println("Output err", err)
		fmt.Println(string(res))
	}
	err = os.Remove(filename)
	if err != nil {
		fmt.Println(err)
	}
	return string(res)
}

func privKeyGen() (string, error) {
	cmd := "/usr/bin/wg"
	args := []string{
		"genkey",
	}
	result := exec.Command(cmd, args...)
	res, err := result.Output()
	return string(res), err
}

func GetKeys() (Keys, error) {
	keys := Keys{}
	priv, err := privKeyGen()
	if err != nil {
		return keys, err
	}
	keys.Privkey = strings.Trim(priv, "\n")
	keys.Pubkey = strings.Trim(pubKeyGen(priv), "\n")
	return keys, nil
}

func Help(item string) {
	switch item {
	case "add":
		fmt.Println("Use wg-manger add name")
		break
	case "server":
		fmt.Println("Use wg-manager server ExternalAddress InternalAddress PBXAddress port")
		break
	default:
		fmt.Println("Help is under construction")
		break
	}
}

func IpToInt(ip string) uint32 {
	netIp := net.ParseIP(ip)
	if len(netIp) == 16 {
		return binary.BigEndian.Uint32(netIp[12:16])
	}
	return binary.BigEndian.Uint32(netIp)
}

func IntToIp(intIp uint32) string {
	ip := make(net.IP, 4)
	binary.BigEndian.PutUint32(ip, intIp)
	return ip.String()
}
