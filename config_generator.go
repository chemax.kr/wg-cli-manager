package main

import (
	"fmt"
	"gorm.io/gorm"
	"os/exec"
)

func peerConfigForServer(peer Peer) string {

	tmplt := `
# client name %s
[Peer]
PublicKey = %s
AllowedIPs = %s,%s/24
`

	return fmt.Sprintf(tmplt, peer.Name, peer.PubKey, IntToIp(peer.VpnIpV4), IntToIp(peer.LocalNet))
}

func interfaceConfigForServer(server ServerConfig) string {
	tmplt := `[Interface]
ListenPort = %d
PrivateKey = %s
Address = %s
`
	//config := "[Interface]"
	//config += fmt.Sprintf("\nListenPort = %d", server.Port)
	//config += fmt.Sprintf("\nPrivateKey = %s", server.PrivKey)
	//config += fmt.Sprintf("\nAddress = %s", IntToIp(server.InternalAddress))
	//config += "\n"
	return fmt.Sprintf(tmplt, server.Port, server.PrivKey, IntToIp(server.InternalAddress))
}

func DoServerConfig(peers []Peer, server ServerConfig) (string, error) {

	serverConfig := interfaceConfigForServer(server)

	for _, peer := range peers {
		serverConfig += peerConfigForServer(peer)
	}

	return serverConfig, nil
}

func DoClientNetworkConfig(peer Peer) (string, error) {
	tmplt := `auto eth0:1
	iface eth0:1 inet static
	address %s
	netmask 255.255.255.0
`
	//config := "\nauto eth0:1"
	//config += "\n\tiface eth0:1 inet static"
	//config += "\n\taddress " + IntToIp(peer.LocalNet+1)
	//config += "\n\tnetmask 255.255.255.0"
	//config += ""
	return fmt.Sprintf(tmplt, IntToIp(peer.LocalNet+1)), nil
}

func DoClientConfig(peer Peer, server ServerConfig) (string, error) {

	tmplt := `[Interface]
ListenPort = %d
PrivateKey = %s
Address = %s

[Peer]
PublicKey = %s
AllowedIPs = %s,%s
Endpoint = %s:%d
PersistentKeepalive = 5
`
	//config := "[Interface]"
	//config += fmt.Sprintf("\nListenPort = %d", peer.Port)
	//config += fmt.Sprintf("\nPrivateKey = %s", peer.PrivKey)
	//config += fmt.Sprintf("\nAddress = %s", IntToIp(peer.VpnIpV4))
	//config += "\n\n"
	//
	//config += "[Peer]"
	//config += fmt.Sprintf("\nPublicKey = %s", server.PubKey)
	//config += fmt.Sprintf("\nAllowedIPs = %s,%s", IntToIp(server.InternalAddress), IntToIp(server.PBXAddress))
	//config += fmt.Sprintf("\nEndpoint = %s:%d", IntToIp(server.Address), server.Port)
	//config += "\nPersistentKeepalive = 5"
	//config += "\n"

	return fmt.Sprintf(tmplt, peer.Port, peer.PrivKey, IntToIp(peer.VpnIpV4), server.PubKey, IntToIp(server.InternalAddress), IntToIp(server.PBXAddress), IntToIp(server.Address), server.Port), nil
}

func ApplyConfig() error {
	cmd := "sh"
	args := []string{"-c", "wg-quick down wg0; wg-quick up wg0"}
	result := exec.Command(cmd, args...)
	_, err := result.Output()

	if err != nil {
		if err, ok := err.(*exec.ExitError); ok {
			fmt.Println(string(err.Stderr))
		}
		return fmt.Errorf("apply config error: %#q", err)
	}
	return nil
}

func GenerateServerConfig(db *gorm.DB) {
	var peers []Peer
	db.Find(&peers)
	var server ServerConfig
	db.Last(&server)
	config, err := DoServerConfig(peers, server)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(config)
	}
	err = SaveConfig(config)
	if err != nil {
		fmt.Println(err)
	} else {
		err := ApplyConfig()
		if err != nil {
			fmt.Println(err)
		}
	}
}

func PeerConfig(peer Peer, server ServerConfig) error {
	config, err := DoClientConfig(peer, server)
	if err != nil {
		return err
	} else {
		fmt.Println(config)
	}
	netConfig, err := DoClientNetworkConfig(peer)
	if err != nil {
		return err
	} else {
		fmt.Println(netConfig)
	}
	err = SavePeerConfigs(peer.Name, config, netConfig)
	if err != nil {
		return err
	}
	return nil
}

func GeneratePeerConfig(name string, db *gorm.DB) {
	var peer Peer
	db.Where("name = ?", name).Find(&peer)
	var server ServerConfig
	db.Last(&server)
	err := PeerConfig(peer, server)
	if err != nil {
		fmt.Println(err)
	}
}
