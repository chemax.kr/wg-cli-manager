package main

import (
	"fmt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"os"
)

type ServerConfig struct {
	gorm.Model
	ID              int64 `gorm:"primaryKey"`
	PrivKey         string
	PubKey          string
	Address         uint32
	InternalAddress uint32
	PBXAddress      uint32
	Port            uint
}

type Peer struct {
	gorm.Model
	ID       int64  `gorm:"primaryKey"`
	Name     string `gorm:"index:unique"`
	PrivKey  string
	PubKey   string
	VpnIpV4  uint32
	LocalNet uint32
	Port     uint
}

const StarterLocalNet = 176619520

func main() {
	db, err := gorm.Open(sqlite.Open("./peers.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	// Migrate the schema
	err = db.AutoMigrate(&ServerConfig{})
	if err != nil {
		fmt.Println(err)
	}
	err = db.AutoMigrate(&Peer{})
	if err != nil {
		fmt.Println(err)
	}
	Route(os.Args, db)

}
