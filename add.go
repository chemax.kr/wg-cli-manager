package main

import (
	"fmt"
	"gorm.io/gorm"
	"os/exec"
	"strconv"
)

func CreateServer(Address, InternalAddress, PBXAddress, port string, db *gorm.DB) {
	keys, err := GetKeys()
	if err != nil {
		fmt.Println(fmt.Errorf("got error when generate keys: %#q", err))
	}
	portU, _ := strconv.Atoi(port)
	server := ServerConfig{
		PrivKey:         keys.Privkey,
		PubKey:          keys.Pubkey,
		Address:         IpToInt(Address),
		InternalAddress: IpToInt(InternalAddress),
		PBXAddress:      IpToInt(PBXAddress),
		Port:            uint(portU),
	}
	fmt.Println("Create server:")
	fmt.Printf("endtypoint:\t%s\n", IntToIp(server.Address))
	fmt.Printf("Address:\t%s\n", IntToIp(server.InternalAddress))
	fmt.Printf("PBX:\t\t%s\n", IntToIp(server.PBXAddress))
	err = db.Create(&server).Error
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("\nServer created:")
	fmt.Printf("endtypoint:\t%s:%d\n", IntToIp(server.Address), server.Port)
	fmt.Printf("Address:\t%s\n", IntToIp(server.InternalAddress))
	fmt.Printf("PBX:\t\t%s\n", IntToIp(server.PBXAddress))
	fmt.Println("")
}

func AddRoutes(peer Peer) error {
	cmd := "sh"
	routeCmd := fmt.Sprintf("ip route add %s/24 dev wg0 via %s", IntToIp(peer.LocalNet), IntToIp(peer.VpnIpV4))
	args := []string{"-c", routeCmd}
	result := exec.Command(cmd, args...)
	_, err := result.Output()
	if err != nil {
		return fmt.Errorf("apply route config error: %#q", err)
	}
	return nil
}

func AddPeer(name string, db *gorm.DB) {
	var lastPeer Peer
	var server ServerConfig
	var LocalNet uint32
	var vpnIpV4 uint32
	db.Last(&server)
	result := db.Last(&lastPeer)
	if result.RowsAffected == 0 {
		vpnIpV4 = server.InternalAddress + 1
		LocalNet = StarterLocalNet + 256
	} else {
		vpnIpV4 = lastPeer.VpnIpV4 + 1
		LocalNet = lastPeer.LocalNet + 256
	}
	keys, err := GetKeys()

	if err != nil {
		fmt.Println(fmt.Errorf("got error when generate keys: %#q", err))
	} else {
		peer := Peer{
			VpnIpV4:  vpnIpV4,
			Name:     name,
			LocalNet: LocalNet,
			PubKey:   keys.Pubkey,
			PrivKey:  keys.Privkey,
			Port:     server.Port,
		}
		createRes := db.Create(&peer)
		fmt.Println(createRes)
		fmt.Println(peer.ID)
		err := AddRoutes(peer)
		if err != nil {
			fmt.Println(err)
		}
		err = PeerConfig(peer, server)
		if err != nil {
			fmt.Println(err)
		}
	}
}
