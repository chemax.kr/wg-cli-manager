package main

import (
	"fmt"
	"gorm.io/gorm"
)

func Show(db *gorm.DB) {
	var peers []Peer
	db.Find(&peers)
	fmt.Print("ID\tName\tAddress\t\tNetwork")
	for i, peer := range peers {
		fmt.Printf("\n%d\t%s\t%s\t%s/24", i, peer.Name, IntToIp(peer.VpnIpV4), IntToIp(peer.LocalNet))
	}
	fmt.Println("")
}
