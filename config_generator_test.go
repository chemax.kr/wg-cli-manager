package main

import (
	"testing"
)

func TestDoServerConfig(t *testing.T) {
	want := `[Interface]
ListenPort = 50000
PrivateKey = 
Address = 10.10.10.1

# client name enot
[Peer]
PublicKey = 654321
AllowedIPs = 10.200.200.2,10.222.0.0/24

# client name enot
[Peer]
PublicKey = 654321
AllowedIPs = 10.200.200.2,10.222.0.0/24
`
	var peers []Peer
	peers = append(peers, Peer{Name: "enot",
		PrivKey:  "123456",
		PubKey:   "654321",
		VpnIpV4:  180930562,
		LocalNet: 182321152,
		Port:     50000,
	})
	peers = append(peers, Peer{Name: "enot",
		PrivKey:  "123456",
		PubKey:   "654321",
		VpnIpV4:  180930562,
		LocalNet: 182321152,
		Port:     50000,
	})
	server := ServerConfig{PubKey: "098765",
		PBXAddress:      177602562,
		InternalAddress: 168430081,
		Address:         16843009,
		Port:            50000,
	}
	config, err := DoServerConfig(peers, server)
	if err != nil {
		t.Fatalf("Error in DoClientConfig: %#q", err)
	}
	if want != config {
		t.Fatalf("Exptected \n======\n%s\n=====\ngot  \n======\n%s\n=====", want, config)
	}
}

func TestDoClientConfig(t *testing.T) {
	want := `[Interface]
ListenPort = 50000
PrivateKey = 123456
Address = 10.200.200.2

[Peer]
PublicKey = 098765
AllowedIPs = 10.10.10.1,10.150.0.2
Endpoint = 1.1.1.1:50000
PersistentKeepalive = 5
`
	peer := Peer{Name: "enot",
		PrivKey:  "123456",
		PubKey:   "654321",
		VpnIpV4:  180930562,
		LocalNet: 182321152,
		Port:     50000,
	}
	server := ServerConfig{PubKey: "098765",
		PBXAddress:      177602562,
		InternalAddress: 168430081,
		Address:         16843009,
		Port:            50000,
	}
	peerConfig, err := DoClientConfig(peer, server)
	if err != nil {
		t.Fatalf("Error in DoClientConfig: %#q", err)
	}
	if want != peerConfig {
		t.Fatalf("Exptected \n======\n%s\n=====\ngot  \n======\n%s\n=====", want, peerConfig)
	}
}
