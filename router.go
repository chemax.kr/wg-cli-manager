package main

import (
	"gorm.io/gorm"
)

func Route(args []string, db *gorm.DB) {
	switch args[1] {
	case "show":
		Show(db)
		break
	case "server":
		if len(args) != 6 {
			Help("server")
		} else {
			CreateServer(args[2], args[3], args[4], args[5], db)
		}
		//CreateServer()
		break
	case "add":
		if len(args) != 3 {
			Help("add")
		} else {
			AddPeer(args[2], db)
		}
		break
	case "delete":
		if len(args) != 3 {
			Help("add")
		} else {
			Delete(args[2], db)
		}
		break
	case "generate":
		GenerateServerConfig(db)
		break
	case "peerconfig":
		if len(args) != 3 {
			Help("peerconfig")
		} else {
			GeneratePeerConfig(args[2], db)
		}
		break
	default:
		Help("")
		break
	}
}
