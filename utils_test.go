package main

import "testing"

func TestIntToIpSuccess(t *testing.T) {
	var ip uint32 = 180930562
	want := "10.200.200.2"
	strIp := IntToIp(ip)
	if strIp != want {
		t.Fatalf("Incorrect Decimal to IP convert! %d convert to \"%s\", when whant \"%s\"", ip, strIp, want)
	}
}

func TestIpToInt(t *testing.T) {
	ip := "10.10.10.1"
	var want uint32 = 168430081
	ipDec := IpToInt(ip)
	if want != ipDec {
		t.Fatalf("Inccorrect IP to Decimal convert! Want %d but get %d", want, ipDec)
	}
}
