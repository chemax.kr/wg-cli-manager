package main

import (
	"fmt"
	"os"
)

func SavePeerConfigs(name string, config string, config2 string) error {
	dirname := fmt.Sprintf("/opt/clients/%s/", name)
	err := os.MkdirAll(dirname, 0744)
	if err != nil {
		return fmt.Errorf("error when create dir for client: %#q", err)
	}
	err = os.MkdirAll(dirname+"etc/wireguard", 0644)
	if err != nil {
		return fmt.Errorf("error when create wireguard dir for client: %#q", err)
	}
	err = os.MkdirAll(dirname+"etc/network/interfaces.d/", 0644)
	if err != nil {
		return fmt.Errorf("error when create interfaces dir for client: %#q", err)
	}
	configFile := dirname + "etc/wireguard/wg0.conf"
	netConfigFile := dirname + "etc/network/interfaces.d/eth0"
	err = os.WriteFile(configFile, []byte(config), 0644)
	if err != nil {
		return fmt.Errorf("error when write config for client: %#q", err)
	}
	err = os.WriteFile(netConfigFile, []byte(config2), 0644)
	if err != nil {
		return fmt.Errorf("error when write netconfig for client: %#q", err)
	}
	return nil
}

func SaveConfig(config string) error {
	filename := "/etc/wireguard/wg0.conf"
	content := []byte(config)
	err := os.WriteFile(filename, content, 0644)
	if err != nil {
		return err
	}
	return nil
}
